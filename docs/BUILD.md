REF:
- <https://gitlab.com/kalilinux/nethunter/apps/kali-nethunter-store-client/-/blob/nethunter/docs/BUILD.md>
- <https://gitlab.com/kalilinux/nethunter/apps/kali-nethunter-store-privileged-extension/-/blob/nethunter/docs/BUILD.md>

- - -

On the host, get ready with [fdroidserver](https://github.com/f-droid/fdroidserver):

```console
cd /srv/nethunter-store/fdroidserver/buildserver/; su builder
vagrant up
vagrant ssh-config
vagrant ssh
```

- - -

On the guest (aka builder VM), setup network apt, as Debian docker/fdroid CI doesn't enable `contrib` (which is needed for `sdkmanager`):

```console
echo "deb https://deb.debian.org/debian/ sid main" | sudo tee /etc/apt/sources.list.d/unstable.list
sudo sed -i 's/main$/main contrib non-free non-free-firmware/g' /etc/apt/sources.list /etc/apt/sources.list.d/*
sudo apt-get update
```

- - - 

Debian sid (aka Unstable), has more/all? Java versions (otherwise, could use `temurin-*-jdk`):

```console
apt-cache search openjdk | awk '/openjdk-[0-9]*-jdk / {print $1}'
sudo apt-get install --yes openjdk-8-jdk
update-alternatives --list java
sudo update-alternatives --set java /usr/lib/jvm/java-8-openjdk-amd64/jre/bin/java
update-alternatives --list javac
sudo update-alternatives --set javac /usr/lib/jvm/java-8-openjdk-amd64/bin/javac
```

<!--
            = Java Major version 45 = Java 1.1
            = Java Major version 46 = Java 1.2
            = Java Major version 47 = Java 1.3
            = Java Major version 48 = Java 1.4
            = Java Major version 49 = Java 5
            = Java Major version 50 = Java 6
            = Java Major version 51 = Java 7  - openjdk-7 - Debian 8 (Jessie)
Gradle 2.0  = Java Major version 52 = Java 8  - openjdk-8 - Debian 9 (Stretch)
Gradle 4.3  = Java Major version 53 = Java 9
Gradle 4.7  = Java Major version 54 = Java 10
Gradle 5.0  = Java Major version 55 = Java 11 - openjdk-11 - Debian 11 (bullseye) & Debian 10 (buster)
Gradle 5.4  = Java Major version 56 = Java 12
Gradle 6.0  = Java Major version 57 = Java 13
Gradle 6.3  = Java Major version 58 = Java 14
Gradle 6.7  = Java Major version 59 = Java 15
Gradle 7.0  = Java Major version 60 = Java 16
Gradle 7.3  = Java Major version 61 = Java 17 - openjdk-17 - Debian 12 (bookworm) & Debian 11 (bullseye)
Gradle 7.5  = Java Major version 62 = Java 18
Gradle 7.6  = Java Major version 63 = Java 19
Gradle 8.3  = Java Major version 64 = Java 20
Gradle 8.5  = Java Major version 65 = Java 21
Gradle 8.8  = Java Major version 66 = Java 22
Gradle 8.10 = Java Major version 67 = Java 23

REF: https://wiki.debian.org/Java
     https://docs.gradle.org/current/userguide/compatibility.html
-->

- - -

Install Android SDK:

```console
sudo apt-get install --yes android-sdk sdkmanager
grep -q ANDROID_HOME ~/.bashrc || echo export ANDROID_HOME=/usr/lib/android-sdk >> ~/.bashrc       # Fix: > The SDK directory '/opt/android-sdk' does not exist.
grep -q ANDROID_SDK_ROOT ~/.bashrc || echo export ANDROID_SDK_ROOT=\${ANDROID_HOME} >> ~/.bashrc   # Fix: > SDK location not found. Define location with an ANDROID_SDK_ROOT environment variable..
source ~/.bashrc
sudo chown -v $(whoami) ${ANDROID_HOME}/{,build-tools,platforms,licenses}             # Not a fan of this. Someone please school me!
```

_Feels dirty, but working (and we are in a VM!)_

- - -

Get the Android app (Kali NetHunter Store - aka HackStore):

```console
sudo apt-get install --yes git
git clone https://gitlab.com/kalilinux/nethunter/apps/kali-nethunter-store-client.git
cd kali-nethunter-store-client/
```
- - -

Finish up with SDK/app's packages/dependencies:

```console
COMPILED_SDK=$( sed -n 's_.*compileSdkVersion\s*\([0-9]*\).*_\1_p' ./app/build.gradle )
BUILD_TOOLS=$( sed -n "s_.*buildToolsVersion\s*'\([0-9.]*\)'.*_\1_p" ./app/build.gradle )
sdkmanager --verbose "build-tools;${BUILD_TOOLS}" "platforms;android-${COMPILED_SDK}"
yes | sdkmanager --verbose  --licenses
```

<!-- alt: $ apt install android-sdk-platform-23 -->

- - -

Build:

```console
./gradlew assembleDebug
./gradlew assembleDebug --info
find . -name '*apk' -type f
./gradlew assembleRelease
find $( pwd ) -name '*release*.apk' -type f ! -name '*basic*'
```

<!-- $ ./gradlew assembleDebug != $ gradle assembleDebug -->

- - - 

Exit VM, and copy out the build output (Android app, `*.apk`):

```console
exit
vagrant plugin install vagrant-scp
vagrant scp default:/home/vagrant/kali-nethunter-store-client/app/build/outputs/apk/fullNetHunter/release/app-full-NetHunter-release-unsigned.apk /tmp/app-full-NetHunter-release-unsigned.apk
```

_Using Vagrant plugin, [vagrant-scp](https://github.com/invernizzi/vagrant-scp) to make life easier._
